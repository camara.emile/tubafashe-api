import { Entity, Column, ObjectIdColumn, ObjectID, CreateDateColumn } from 'typeorm';
import { IsEmail } from 'class-validator';

@Entity({
  name: 'donator',
})
export class Donator {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({
    length: 50,
  })
  lastName: string;

  @Column({
    length: 50,
  })
  firstName: string;

  @Column({
    length: 100,
  })
  @IsEmail()
  email: string;

  @Column({
    length: 20,
  })
  mobilPhone: string;

  @Column({
    length: 100,
    nullable: true,
  })
  companyName: string;

  @Column({
    type: 'text',
  })
  address: string;

  @Column({
    type: 'text',
  })
  additionalAddress: string;

  @Column({
    length: 50,
  })
  city: string;

  @Column({
    length: 50,
  })
  country: string;

  @Column({
    length: 10,
  })
  zip: string;

  @CreateDateColumn()
  createdAt: string;

}
