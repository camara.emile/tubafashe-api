import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Donator } from '../entity/donator.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { PaymentRequestDto } from '../../payment/dto/payment-request.dto';

@Injectable()
export class DonatorService {
  constructor( @InjectRepository(Donator)
               private readonly donatorRepository: Repository<Donator>) {}

  public async findAll(): Promise<Donator[]> {
    return this.donatorRepository.find();
  }

  public async findByPk(donatorID: string): Promise<Donator> {
    return await this.donatorRepository.findOne(donatorID);
  }

  public async save(donator: Donator): Promise<Donator> {
    return await this.donatorRepository.save(donator);
  }

  public async buildAndSaveDonator(kpayPaymentRequestDto: PaymentRequestDto): Promise<Donator> {
    const donator: Donator = new Donator();
    donator.lastName = kpayPaymentRequestDto.lastName;
    donator.firstName = kpayPaymentRequestDto.firstName;
    donator.email = kpayPaymentRequestDto.email;
    donator.address = kpayPaymentRequestDto.address;
    donator.additionalAddress = kpayPaymentRequestDto.additionalAddress;
    donator.city = kpayPaymentRequestDto.city;
    donator.companyName = kpayPaymentRequestDto.companyName;
    donator.mobilPhone = kpayPaymentRequestDto.mobilPhone;

    return await this.donatorRepository.save(donator);
  }
}
