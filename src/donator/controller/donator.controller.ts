import { DonatorService } from '../service/donator.service';
import { Donator } from '../entity/donator.entity';
import { Controller, Get } from '@nestjs/common';

@Controller('donators')
export class DonatorController {
  constructor(private readonly donatorService: DonatorService) {}

  @Get()
  findAll(): Promise<Donator[]> {
    return this.donatorService.findAll();
  }
}
