import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class CreateDonatorDto {
  id: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  firstName: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  lastName: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  email: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  mobilPhone: string | '';

  @ApiProperty({
    nullable: true,
    type: String,
  })
  companyName: string | '';

  @ApiProperty({
    nullable: true,
    type: String,
  })
  address: string | '';

  @ApiProperty({
    nullable: true,
    type: String,
  })
  additionalAddress: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  city: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  country: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  zip: string;
}
