import { Module } from '@nestjs/common';
import { DonatorService } from './service/donator.service';
import { Donator } from './entity/donator.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DonatorController } from './controller/donator.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Donator])],
  providers: [
    DonatorService,
  ],
  exports: [DonatorService],
  controllers: [DonatorController],
})
export class DonatorModule {}
