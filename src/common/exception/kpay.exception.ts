import { HttpException, HttpStatus } from '@nestjs/common';

export class KpayException extends HttpException {
  constructor(message?: string) {
    super({
      status: HttpStatus.EXPECTATION_FAILED,
      error: (message) ? message : 'NOT FOUND',
      date: new Date().toISOString(),
    }, HttpStatus.EXPECTATION_FAILED);
  }
}
