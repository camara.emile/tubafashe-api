import { HttpException, HttpStatus } from '@nestjs/common';

export class BadRequestException extends HttpException {
  constructor(message?: string) {
    super({
      status: HttpStatus.FORBIDDEN,
      error: (message) ? message : 'FORBIDDEN',
      date: new Date().toISOString(),
    }, HttpStatus.FORBIDDEN);
  }
}
