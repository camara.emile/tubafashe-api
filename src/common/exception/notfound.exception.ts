import { ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';

export class NotFoundException extends HttpException {
  constructor(message?: string) {
    super({
      status: HttpStatus.NOT_FOUND,
      error: (message) ? message : 'NOT FOUND',
      date: new Date().toISOString(),
    }, HttpStatus.NOT_FOUND);
  }
}
