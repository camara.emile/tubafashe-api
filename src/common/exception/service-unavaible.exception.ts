import { HttpException, HttpStatus } from '@nestjs/common';

export class ServiceUnavaibleException extends HttpException {
  constructor(message?: string) {
    super({
      status: HttpStatus.SERVICE_UNAVAILABLE,
      error: (message) ? message : 'Service unavaible',
      date: new Date().toISOString(),
    }, HttpStatus.SERVICE_UNAVAILABLE);
  }
}
