import * as Joi from 'joi';
import { env, validate } from './env-validator.decorator';

export class ConfigBucket {
  @env(Joi.string().required())
  public readonly DATABASE_HOST: string;

  @env(Joi.string().required())
  public readonly DATABASE_NAME: string;

  public readonly DATABASE_USER: string;

  public readonly DATABASE_PWD: string;

  @env(Joi.string().required())
  public readonly KPAY_LOGIN: string;

  @env(Joi.string().required())
  public readonly KPAY_PWD: string;

  @env(Joi.string().required())
  public readonly KPAY_URL_PAYMENT: string;

  @env(Joi.string().required())
  public readonly KPAY_RETURL: string;

  @env(Joi.string().required())
  public readonly KPAY_REDIRECTURL: string;

  @env(Joi.string().required())
  public readonly KPAY_RETAILERID: string;

  @env(Joi.string().required())
  public readonly KPAY_BANKID: string;

  @env(Joi.string().required())
  public readonly DATA_FIXER_API_URL: string;

  @env(Joi.string().required())
  public readonly DATA_FIXER_API_KEY: string;

  public readonly FOREX_API_URL: string = 'https://www.freeforexapi.com/api/';

  @env(Joi.string().required())
  public readonly JWK_URI: string;

  @env(Joi.string().required())
  public readonly JWT_AUDIENCE: string;

  @env(Joi.string().required())
  public readonly JWT_ISSUER: string;

  @env(Joi.string().required())
  MAILER_SERVICE: string;

  @env(Joi.string().required())
  MAILER_USER: string;

  @env(Joi.string().required())
  MAILER_PASS: string;

  public readonly REST_COUNTRIES_API: string = 'https://restcountries.herokuapp.com/api/v1';

  public readonly TOKEN_SECRET: string = '6FYUCKKUAU';
}

export const conf = validate<ConfigBucket>(ConfigBucket);
