import { Global, Module } from '@nestjs/common';
import { conf as configBucket, ConfigBucket } from './config-bucket';
import { AppLogger } from './app-logger';

@Global()
@Module({
  providers: [
    {
      provide: ConfigBucket, useValue: configBucket,
    },
    AppLogger,
  ],
  exports: [
    ConfigBucket,
    AppLogger,
    ],
})
export class CommonModule {
}
