export const DATABASE_CONNECTION = 'DATABASE_CONNECTION';
export const PAYMT_PENDING = 'PAYMENT PENDING';
export const PAYMT_VALIDED = 'Approved';
export const PAYMT_CANCELED = 'PAYMENT CANCELED';
export const PAYMT_TRANSACTION_FAILED = 'Transaction failed';
export const KPAY_PAYMENT = 'KPAY';
export const MAIL_FROM = 'murakoze@tubafashe.rw';
export const MAIL_SUBJECT = 'Votre don sur le site Tubafashe';
