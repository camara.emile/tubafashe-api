import * as joi from 'joi';
import * as _ from 'lodash';
import 'reflect-metadata';

const envMetadataKey = Symbol('env');

export function env(schema: joi.Schema): (target: object, propertyKey: string) => void {
  return (target: object, propertyKey: string) => {
    // Build a list of env params
    const existingEnvParams = Reflect.getMetadata(envMetadataKey, target) || {};
    existingEnvParams[propertyKey] = schema;
    Reflect.defineMetadata(envMetadataKey, existingEnvParams, target);

    // property getter
    const getter = () => {
      return process.env[propertyKey];
    };

    // Affect new property.
    if (delete target[propertyKey]) {
      // Create new property with getter and setter
      Object.defineProperty(target, propertyKey, {
        get: getter,
        enumerable: true,
        configurable: true,
      });
    }
  };
}
export type Type<T> = new () => T;
export function validate<T>(configBucket: Type<T>): T {
  const objConfigBucket = typeof configBucket === 'function' ? new configBucket() : configBucket;
  const meta = Reflect.getMetadata(envMetadataKey, objConfigBucket);
  const values = _.transform(Object.keys(meta), (result: object, key: string) => {
    result[key] = process.env[key];
  }, {});
  const validationResult: joi.ValidationResult<T> = joi.validate({...objConfigBucket, ...values},
      meta, {abortEarly: false, allowUnknown: true});
  if (validationResult.error) {
    const details = ['Invalid environment configuration'];
    _.transform(validationResult.error.details, (acc: string[], detail: joi.ValidationErrorItem) => {
      return acc.push('Environment variable ' + detail.message);
    }, details);
    throw new Error(details.join('\n'));
  }

  return Object.freeze(validationResult.value as T);
}
