import { NestMiddleware } from '@nestjs/common';
import * as jwt from 'express-jwt';
import { expressJwtSecret } from 'jwks-rsa';
import { ConfigBucket } from './config-bucket';
import { validate } from './env-validator.decorator';

const configBucket: ConfigBucket = validate<ConfigBucket>(ConfigBucket);

export class AuthenticationMiddleware implements NestMiddleware {
  use(req, res, next) {
    jwt({
      secret: expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: configBucket.JWK_URI,
      }),
      audience: configBucket.JWT_AUDIENCE,
      issuer: configBucket.JWT_ISSUER,
      algorithm: 'RS256',
    })(req, res, err => {
      if (err) {
        const status = err.status || 500;
        const message =
          err.message || 'Sorry, we were unable to process your request.';
        return res.status(status).send({
          message,
        });
      }
      next();
    });
  }
}
