import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './common/exception/http-exception.filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
async function bootstrap(): Promise<void> {

  const corsOptions = {
    origin: ['https://tubafashe.dev.teradig.rw','https://dev.tubafashe.rw', 'http://localhost:4200', 'https://tubafashe-22380.web.app'],
    methods: ['GET', 'PUT', 'POST', 'OPTIONS', 'DELETE'],
    allowedHeaders: ['DNT','User-Agent','X-Requested-With','If-Modified-Since','Cache-Control','Content-Type','Range','Authorization'],
    exposedHeaders: ['Content-Length','Content-Range'],
    credentials: true,
    preflightContinue: false,
    optionsSuccessStatus: 204
  }

  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new HttpExceptionFilter());
  app.setGlobalPrefix('api');
  app.use(helmet());
  app.enableCors(corsOptions);
  const options = new DocumentBuilder()
  .setTitle('Tubafashe API')
  .setDescription('The Tubafashe API description')
  .setVersion('1.0')
  .addTag('Tubafashe')
  .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
