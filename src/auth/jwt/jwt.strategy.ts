import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { ConfigBucket } from '../../common/config-bucket';
import { User } from '../entity/user.entity';
import { UserRepository } from '../repository/user.repository';
import { JwtPayload } from './jwt-payload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
      @InjectRepository(UserRepository)
      private userRepository: UserRepository,
      private readonly configBucket: ConfigBucket) {
      super({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: configBucket.TOKEN_SECRET,
      });
    }

    async validate(payload: JwtPayload): Promise<User> {
      const { username } = payload;
      const user = await this.userRepository.findOne({ username });

      if (!user) {
        throw new UnauthorizedException();
      }

      return user;
    }
}
