import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentType } from './entity/payment-type.entity';
import { Payment } from './entity/payment.entity';
import { PaymentService } from './service/payment.service';
import { PaymentController } from './controller/payment.controller';
import { KpayService } from './service/kpay.service';
import { CurrencyRate } from './entity/currency-rate.entity';
import { CurrencyRateService } from './service/currency-rate.service';
import { CurrencyController } from './controller/currency.controller';
import { DonatorService } from '../donator/service/donator.service';
import { Donator } from '../donator/entity/donator.entity';
import { Campaign } from '../campaign/entity/campaign.entity';
import { CampaignService } from '../campaign/service/campaign.service';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Campaign, CurrencyRate, Payment, PaymentType, Donator])],
  providers: [
    CampaignService,
    PaymentService,
    KpayService,
    CurrencyRateService,
    DonatorService,
  ],
  controllers: [CurrencyController, PaymentController],
})
export class PaymentModule {}
