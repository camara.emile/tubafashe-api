import { Controller, Get, Param } from '@nestjs/common';
import { CurrencyRateService } from '../service/currency-rate.service';
import { ApiBody } from '@nestjs/swagger';
import { CurrencyRateResponseDto } from '../dto/currency-rate-response.dto';

@Controller('currency-rates')
export class CurrencyController {
  constructor(private readonly currencyRateService: CurrencyRateService) {}

  @Get(':currency')
  @ApiBody({ type: [CurrencyRateResponseDto] })
  async getCurrencyRate(@Param('currency') currency: string): Promise<CurrencyRateResponseDto> {
    return await this.currencyRateService.getCurrencyRate(currency);
  }
}
