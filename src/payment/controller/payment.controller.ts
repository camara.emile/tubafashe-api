import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { PaymentService } from '../service/payment.service';
import { Payment } from '../entity/payment.entity';
import { PaymentRequestDto } from '../dto/payment-request.dto';
import { PaymentResponseDto } from '../dto/payment.response.dto';
import { KpayCallbackResponseDto } from '../dto/kpay-callback.response.dto';
import { ApiBody } from '@nestjs/swagger';

@Controller('payments')
export class PaymentController {
  constructor(private readonly paymentService: PaymentService) {}

  @Get()
  @ApiBody({ type: [Payment] })
  findAll(): Promise<Payment[]> {
    return this.paymentService.findAll();
  }

  @Get('/purge')
  remove(@Query() params): Promise<void> {
    return this.paymentService.removePayments(params.amount);
  }

  @Post()
  async sendPayment(@Body() paymentRequestDto: PaymentRequestDto): Promise<PaymentResponseDto> {
    return this.paymentService.sendPayment(paymentRequestDto);
  }

   @Get('check-kpay-status/:refID')
  async paymentCheckStatus(@Param('refID') refID: string) {
    return this.paymentService.paymentCheckStatus(refID);
  }

  @Post('kpay-callback')
  async kpayCallback(@Body() kpayCallbackResponseDto: KpayCallbackResponseDto): Promise<void> {
     return this.paymentService.kpayCallback(kpayCallbackResponseDto);
  }
}
