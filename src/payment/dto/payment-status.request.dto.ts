import { ApiProperty } from '@nestjs/swagger';
import { KpayStatusRequestDto } from './kpay-status.request.dto';

export class PaymentStatusRequestDto {
  @ApiProperty()
  kpayStatusRequestDto?: KpayStatusRequestDto;
}
