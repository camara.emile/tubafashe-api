interface USDRWF {
  rate: number;
  timestamp: number;
}

interface ForexResponse {
  USDRWF: USDRWF;
}

export class ForexCurrencyRateResponseDto {
  rates: ForexResponse;
  code: number;
}
