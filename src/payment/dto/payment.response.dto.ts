export class PaymentResponseDto {
  reply: string;
  url: string;
  success: number;
  authkey: string;
  tid: string;
  refid: string;
  retcode: number;
  error: string;
}
