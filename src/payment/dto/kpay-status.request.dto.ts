import { ApiProperty } from '@nestjs/swagger';

export class KpayStatusRequestDto {
  @ApiProperty({
    nullable: false,
    type: String,
  })
  tid: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  refid: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  action: string;
}
