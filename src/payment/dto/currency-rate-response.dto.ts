import { Rates } from '../entity/interface/rate.interface';
import { ErrorResponse } from '../entity/interface/error-response.interface';
import { ApiProperty } from '@nestjs/swagger';

export class CurrencyRateResponseDto {
  @ApiProperty({
    nullable: true,
    type: Boolean,
  })
  success: boolean;

  @ApiProperty({
    nullable: true,
    type: Number,
  })
  timestamp: number;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  base: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  date: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  rates: Rates;
  error: ErrorResponse | null;
}
