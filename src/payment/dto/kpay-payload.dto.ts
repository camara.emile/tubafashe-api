
export class KpayPayloadDto {
  msisdn: string;
  details: string;
  refid: string;
  amount: number;
  cname: string;
  cnumber: string;
  pmethod: string;
  retailerid: string;
  returl: string;
  redirecturl: string;
  bankid: string;
}
