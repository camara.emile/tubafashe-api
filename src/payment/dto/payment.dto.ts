import { ApiProperty } from '@nestjs/swagger';

export class PaymentDto {
  id: string;
  @ApiProperty({
    nullable: false,
    type: Number,
  })
  amount: number;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  donatorID: string;
  transactionID: string;
  status: string;
  createdAt: string;
}
