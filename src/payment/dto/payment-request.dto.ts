import { ApiProperty } from '@nestjs/swagger';

export class PaymentRequestDto {

  @ApiProperty({
    nullable: false,
    type: String,
  })
  firstName: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  lastName: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  companyName: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  email: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  additionalAddress: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  city: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  country: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  address: string | '';

  @ApiProperty({
    nullable: false,
    type: String,
  })
  mobilPhone: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  campaignID: string;

  @ApiProperty({
    nullable: false,
    type: Number,
  })
  amount: number;

  @ApiProperty({
    nullable: false,
    type: String,
    enum: ['KPAY'],
  })
  paymentName: string;

  @ApiProperty({
    nullable: false,
    type: String,
    enum: ['cc', 'momo'],
  })
  pmethod: string;

  @ApiProperty({
    nullable: false,
    type: String,
    enum: ['USD', 'EUR'],
  })
  currency: string;
}
