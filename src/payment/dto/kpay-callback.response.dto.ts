import { ApiProperty } from '@nestjs/swagger';

export class KpayCallbackResponseDto {
  @ApiProperty({
    nullable: true,
    type: String,
  })
  tid: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  refid: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  momtransactionid: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  statusid: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  statusdesc: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  payaccount: string;

}
