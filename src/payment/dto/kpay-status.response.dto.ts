import { ApiProperty } from '@nestjs/swagger';

export class KpayStatusResponseDto {
  @ApiProperty({
    nullable: false,
    type: String,
  })
  tid: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  refid: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  momtransactionid: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  statusdesc: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  comtime: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  statusid: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  campaignID: string;
}
