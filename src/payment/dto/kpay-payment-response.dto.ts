import { PaymentResponseDto } from './payment.response.dto';

export class KpayPaymentResponseDto extends PaymentResponseDto {
  donatorID: string;
  amount: number;
}
