import { Injectable } from '@nestjs/common';
import { PaymentRequestDto } from '../dto/payment-request.dto';
import got, { Got } from 'got';
import { ConfigBucket } from '../../common/config-bucket';
import { AppLogger } from '../../common/app-logger';
import { KpayPaymentResponseDto } from '../dto/kpay-payment-response.dto';
import { DonatorService } from '../../donator/service/donator.service';
import { Donator } from '../../donator/entity/donator.entity';
import { KpayException } from '../../common/exception/kpay.exception';
import { KpayPayloadDto } from '../dto/kpay-payload.dto';
import { CurrencyRateService } from './currency-rate.service';
import { CurrencyRateResponseDto } from '../dto/currency-rate-response.dto';
import { KpayStatusResponseDto } from '../dto/kpay-status.response.dto';
import { KpayStatusRequestDto } from '../dto/kpay-status.request.dto';

enum Currency { EUR = 'EUR', USD = 'USD', RWF = 'RWF' };
@Injectable()
export class KpayService {
  private readonly kpayApiGot: Got;

  constructor(private readonly configBucket: ConfigBucket,
              private readonly donatorService: DonatorService,
              private readonly currencyRateService: CurrencyRateService,
              private appLogger: AppLogger) {
    this.appLogger.setContext('KpayService');
    this.kpayApiGot = got.extend({
      prefixUrl: this.configBucket.KPAY_URL_PAYMENT,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  public async sendPayment(paymentRequestDto: PaymentRequestDto): Promise<KpayPaymentResponseDto> {
    this.appLogger.log('sendPayment');
    const donator: Donator = await this.donatorService.buildAndSaveDonator(paymentRequestDto);

    return await this.apiCall(paymentRequestDto, donator);
  }

  public async paymentStatus(kpayStatusRequestDto: KpayStatusRequestDto) {
    const kpayStatusResponseDto: KpayStatusResponseDto = await this.kpayApiGot.post('',
      {
        json: kpayStatusRequestDto,
        username: this.configBucket.KPAY_LOGIN,
        password: this.configBucket.KPAY_PWD,
      }).json<KpayStatusResponseDto>();

    return kpayStatusResponseDto;
  }

  private async apiCall(paymentRequestDto: PaymentRequestDto, donator: Donator): Promise<KpayPaymentResponseDto> {
    this.appLogger.log('apiCall');
    const kpayPayloadDto: KpayPayloadDto = await this.buildKpayPayloadDto(paymentRequestDto, donator);
    const kpayPaymentResponseDto: KpayPaymentResponseDto = await this.kpayApiGot.post('',
      {
        json: kpayPayloadDto,
        username: this.configBucket.KPAY_LOGIN,
        password: this.configBucket.KPAY_PWD,
      }).json<KpayPaymentResponseDto>();

    if (kpayPaymentResponseDto.success === 0) {
      throw new KpayException(kpayPaymentResponseDto.reply);
    }
    kpayPaymentResponseDto.donatorID = donator.id.toString();
    kpayPaymentResponseDto.amount = kpayPayloadDto.amount;

    return kpayPaymentResponseDto;
  }

  private async buildKpayPayloadDto(kpayPaymentRequestDto: PaymentRequestDto, donator: Donator): Promise<KpayPayloadDto> {
    const kpayPayloadDto = new KpayPayloadDto();
    kpayPayloadDto.refid = this.refidGenerator(5);
    kpayPayloadDto.details = 'Covid 19 - Donate';
    kpayPayloadDto.retailerid = this.configBucket.KPAY_RETAILERID;
    kpayPayloadDto.returl = this.configBucket.KPAY_RETURL;
    kpayPayloadDto.pmethod = kpayPaymentRequestDto.pmethod;
    kpayPayloadDto.msisdn = kpayPaymentRequestDto.mobilPhone;
    kpayPayloadDto.cname = donator.firstName + ' ' + donator.lastName;
    kpayPayloadDto.amount = await this.convertAmount(kpayPaymentRequestDto.amount,
      kpayPaymentRequestDto.currency);
    kpayPayloadDto.bankid = this.configBucket.KPAY_BANKID;
    kpayPayloadDto.redirecturl = this.configBucket.KPAY_REDIRECTURL + '&refid=' + kpayPayloadDto.refid;
    kpayPayloadDto.cnumber = this.refidGenerator(5);

    return kpayPayloadDto;
  }

  private async checkDonator(donatorID: string): Promise<Donator> {
    return await this.donatorService.findByPk(donatorID);
  }

  private async convertAmount(amount: number, currency: string): Promise<number> {
    if (currency.toUpperCase() !== Currency.EUR && currency.toUpperCase() !== Currency.USD && currency.toUpperCase() !== Currency.RWF) {
      throw new Error('Currency not valid');
    }
    if (currency.toUpperCase() === Currency.RWF) {
      return amount;
    }
    if (currency.toUpperCase() === Currency.EUR || currency.toUpperCase() === Currency.USD) {
      const currencyRate: CurrencyRateResponseDto = await this.currencyRateService.getCurrencyRate(currency);
      const converted: number = +currencyRate.rates.RWF * amount;

      return Number(converted.toFixed());
    }
  }

  private refidGenerator(length: number): string {
    const millis = Date.now();
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result + millis;
  }
}
