import {  Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Payment } from '../entity/payment.entity';
import { PaymentType } from '../entity/payment-type.entity';
import { PaymentRequestDto } from '../dto/payment-request.dto';
import { KpayService } from './kpay.service';
import { KPAY_PAYMENT, MAIL_FROM, MAIL_SUBJECT, PAYMT_PENDING, PAYMT_TRANSACTION_FAILED, PAYMT_VALIDED } from '../../common/constant';
import { KpayPaymentResponseDto } from '../dto/kpay-payment-response.dto';
import { KpayStatusResponseDto } from '../dto/kpay-status.response.dto';
import { NotFoundException } from '../../common/exception/notfound.exception';
import { CampaignService } from '../../campaign/service/campaign.service';
import { KpayCallbackResponseDto } from '../dto/kpay-callback.response.dto';
import { AppLogger } from '../../common/app-logger';
import { KpayStatusRequestDto } from '../dto/kpay-status.request.dto';
import { BadRequestException } from '../../common/exception/bad-request-exception';
import { ConfigBucket } from '../../common/config-bucket';
import { DonatorService } from '../../donator/service/donator.service';
import { Donator } from '../../donator/entity/donator.entity';

@Injectable()
export class PaymentService {
  constructor(@InjectRepository(Payment)
              private readonly paymentRepository: Repository<Payment>,
              @InjectRepository(PaymentType)
              private readonly paymentTypeRepository: Repository<PaymentType>,
              private readonly kpayService: KpayService,
              private readonly campaignService: CampaignService,
              private readonly configBucket: ConfigBucket,
              private readonly donatorService: DonatorService,
              private appLogger: AppLogger,
  ) {
  }

  public async findAll(): Promise<Payment[]> {
    return this.paymentRepository.find();
  }

  public async findPaymentByPk(id: string): Promise<Payment> {
    return this.paymentRepository.findOne(id);
  }

  public async findPaymentTypeByPk(id: string): Promise<PaymentType> {
    return this.paymentTypeRepository.findOne(id);
  }

  public async savePayment(payment: Payment) {
    await this.paymentRepository.save(payment);
  }

  public async sendPayment(paymentRequestDto: PaymentRequestDto): Promise<KpayPaymentResponseDto> {
    if (!paymentRequestDto.campaignID || !paymentRequestDto.paymentName || paymentRequestDto.paymentName !== KPAY_PAYMENT) {
      throw new BadRequestException('Missing parameters');
    }
    if (await this.campaignService.checkCampaign(paymentRequestDto.campaignID) === false) {
      throw new NotFoundException('Campaign with ID: ' + paymentRequestDto.campaignID + ' not found');
    }
    const kpayPaymentResponseDto: KpayPaymentResponseDto = await this.kpayService.sendPayment(paymentRequestDto);
    await this.createPayment(paymentRequestDto, kpayPaymentResponseDto);

    return kpayPaymentResponseDto;
  }

  public async paymentCheckStatus(refID: string): Promise<KpayStatusResponseDto> {
    const payment: Payment = await this.checkPaymentByRefid(refID);
    const kpayStatusResponseDto: KpayStatusResponseDto = await this.kpayService.paymentStatus(PaymentService.buildKpayStatusRequestDto(refID));
    const status: string = PaymentService.getPaymentStatusValue(kpayStatusResponseDto.statusid, kpayStatusResponseDto.statusdesc);
    await this.updatePayment(payment, status);
    kpayStatusResponseDto.campaignID = payment.campaignID;
    return kpayStatusResponseDto;
  }

  public async kpayCallback(kpayCallbackResponseDto: KpayCallbackResponseDto): Promise<void> {
    const payment: Payment = await this.checkPaymentByRefid(kpayCallbackResponseDto.refid);
    const status: string = PaymentService.getPaymentStatusValue(kpayCallbackResponseDto.statusid, kpayCallbackResponseDto.statusdesc);
    await this.updatePayment(payment, status);
  }

  public async removePayments(amount: number): Promise<void> {
    const payments = await this.paymentRepository.find();
    for (const payment of payments) {
      if (payment.amount > amount) {
        await this.paymentRepository.remove(payment);
      }
    }
  }

  private static buildKpayStatusRequestDto(refID: string): KpayStatusRequestDto {
    const kpayStatusRequestDto: KpayStatusRequestDto = new KpayStatusRequestDto();
    kpayStatusRequestDto.refid = refID;
    kpayStatusRequestDto.action = 'checkstatus';

    return kpayStatusRequestDto;
  }

  private async updatePayment(payment: Payment, status: string): Promise<void> {
    payment.status = status || PAYMT_PENDING;
    await this.paymentRepository.save(payment);
    await this.calculateCampaignAmount(payment);
    const donator: Donator = await this.donatorService.findByPk(payment.donatorID);
    if (!payment.mailIsSend) {
      // this.sendMailToDonator(donator.email, payment.status);
      // payment.mailIsSend = true;
      await this.paymentRepository.save(payment);
    }
  }

  private async calculateCampaignAmount(payment: Payment): Promise<void> {
    const amount: number = await this.getTotalAmountByCampaign(payment.campaignID);
    await this.campaignService.updateCampaignAmount(payment, amount);
  }

  private static getPaymentStatusValue(status: string, statusdesc: string): string {
    if (status === '01') {
      return PAYMT_VALIDED;
    } else {
      return statusdesc;
    }
  }

  private async checkPaymentByRefid(refID: string): Promise<Payment> {
    const payment: Payment = await this.paymentRepository.findOne({ refid: refID });
    if (!payment) {
      throw new NotFoundException('Payment not found with RefID:' + refID);
    }
    return payment;
  }

  private async createPayment(paymentRequestDto: PaymentRequestDto, kpayPaymentResponseDto: KpayPaymentResponseDto,
  ): Promise<void> {
    const payment = new Payment();
    payment.amount = kpayPaymentResponseDto.amount;
    payment.authkey = kpayPaymentResponseDto.authkey;
    payment.donatorID = kpayPaymentResponseDto.donatorID;
    payment.paymentTypeID = paymentRequestDto.pmethod;
    payment.refid = kpayPaymentResponseDto.refid;
    payment.tid = kpayPaymentResponseDto.tid;
    payment.status = PAYMT_PENDING;
    payment.campaignID = paymentRequestDto.campaignID;

    await this.paymentRepository.save(payment);
  }

  private async getTotalAmountByCampaign(campaignId: string): Promise<number> {
    let amount: number = 0;
    const payments = await this.paymentRepository.find({ campaignID: campaignId, status: PAYMT_VALIDED });
    if (payments) {
      for ( const payment of payments ) {
        amount = amount + payment.amount;
      }
    }

    return amount;
  }

  private sendMailToDonator(donatorEmail: string, paymentStatus: string) {
    const mailer = require('nodemailer');
    const smtpTransport = mailer.createTransport( {
      service: this.configBucket.MAILER_SERVICE,
      auth: {
        user: this.configBucket.MAILER_USER,
        pass: this.configBucket.MAILER_PASS,
      },
    });

    const message = this.messageBody(paymentStatus);

    const mail = {
      from: MAIL_FROM,
      to: donatorEmail,
      subject: MAIL_SUBJECT,
      html: message,
    };

    smtpTransport.sendMail(mail, (error, response) => {
      if (error) {
        this.appLogger.error(error);
        throw new Error(error);
      }
      this.appLogger.log(response.toString());
      smtpTransport.close();
    });
  }

  private messageBody(status: string): string {
    let message: string;
    if (status === PAYMT_VALIDED) {
      message = 'Bonjour <br/>,' +
        '        Votre don a bien été enregistré. Nous vous remercions pour votre participation.<br/>' +
        '        Cordialement, <br/>' +
        '        L\'équipe Tubafashe.';
    } else {
      message = 'Bonjour <br/>, ' +
        'Votre paiement n\'a pas été effectué. Vous pouvez effectuer un autre don en vous rendant sur notre site web.<br/>' +
        'Cordialement, <br/>' +
        'L\'équipe Tubafashe.';
    }

    return message;
  }

}
