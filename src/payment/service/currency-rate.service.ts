import { Injectable } from '@nestjs/common';
import { CurrencyRateResponseDto } from '../dto/currency-rate-response.dto';
import got, { Got } from 'got';
import { ConfigBucket } from '../../common/config-bucket';
import { AppLogger } from '../../common/app-logger';
import { InjectRepository } from '@nestjs/typeorm';
import { CurrencyRate } from '../entity/currency-rate.entity';
import { Repository } from 'typeorm';
import { ServiceUnavaibleException } from '../../common/exception/service-unavaible.exception';
import { ForexCurrencyRateResponseDto } from '../dto/forex-currency-rate-response.dto';
import { Rates } from '../entity/interface/rate.interface';

@Injectable()
export class CurrencyRateService {
  private readonly dataFixerApiGot: Got;
  private readonly forexApiUrlGot: Got;
  constructor(private readonly configBucket: ConfigBucket,
              private appLogger: AppLogger,
              @InjectRepository(CurrencyRate)
              private readonly currencyRateRepository: Repository<CurrencyRate>,
  ) {
    this.appLogger.setContext('CurrencyRateService');
    this.dataFixerApiGot = got.extend({
      prefixUrl: this.configBucket.DATA_FIXER_API_URL,
      headers: {
        'Content-Type': 'application/json',
      },
    });
    this.forexApiUrlGot = got.extend({
      prefixUrl: this.configBucket.FOREX_API_URL,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  public async getCurrencyRate(currency: string): Promise<CurrencyRateResponseDto> {
    try {
      this.appLogger.log('getCurrencyRate');
      switch (currency) {
        case 'EUR':
          return this.getCurrencyRateFromDataFixer();
        case 'USD':
          return this.getLatestCurrencyRateFromForexApi();
      }

    } catch (e) {
      this.appLogger.error('getCurrencyRate: ' + e.toString());
    }
  }

  private async getCurrencyRateFromDataFixer() {
    const currencyRate: CurrencyRate = await this.currencyRateRepository.findOne({value: 'EURtoRWFandUSD'});
    if (currencyRate) {
      const numberOfDaysSinceLastUpd = CurrencyRateService.getDiffBetween2Dates(currencyRate.updateAt);
      if (numberOfDaysSinceLastUpd > 1) {
        return this.getLatestCurrencyRate();
      } else {
        const currencyRateResponseDto = new CurrencyRateResponseDto();
        currencyRateResponseDto.rates = currencyRate.rates;
        return currencyRateResponseDto;
      }
    } else {
      return this.getLatestCurrencyRate();
    }
  }

  private async getLatestCurrencyRate(): Promise<CurrencyRateResponseDto> {
    this.appLogger.log('getLatestCurrencyRate');
    const url = '/latest?access_key=' + this.configBucket.DATA_FIXER_API_KEY + '&symbols=USD,XOF,RWF';
    const response: CurrencyRateResponseDto = await this.dataFixerApiGot.get(url).json<CurrencyRateResponseDto>();
    if (!response.success) {
        throw new ServiceUnavaibleException(response.error.info);
    }
    if (response.success === true) {
        const newCurrencyRate: CurrencyRate = new CurrencyRate();
        newCurrencyRate.value = 'EURtoRWFandUSD';
        newCurrencyRate.rates = response.rates;
        await this.currencyRateRepository.save(newCurrencyRate);
        return response;
    }
  }

  private async getLatestCurrencyRateFromForexApi(): Promise<CurrencyRateResponseDto> {
    this.appLogger.log('getLatestCurrencyRateFromForexApi');
    const currencyRate: CurrencyRate = await this.currencyRateRepository.findOne({value: 'USDtoRWF'});
    if (currencyRate) {
      const numberOfDaysSinceLastUpd = CurrencyRateService.getDiffBetween2Dates(currencyRate.updateAt);
      if (numberOfDaysSinceLastUpd > 1) {
        return await this.forexApiCall();
      } else {
        const currencyRateResponseDto = new CurrencyRateResponseDto();
        currencyRateResponseDto.rates = currencyRate.rates;
        return currencyRateResponseDto;
      }
    }  else {
      return await this.forexApiCall();
    }

  }

  private async forexApiCall(): Promise<CurrencyRateResponseDto> {
    const response: ForexCurrencyRateResponseDto = await this.forexApiUrlGot.get('live?pairs=USDRWF').json<ForexCurrencyRateResponseDto>();
    if (response.code === 200) {
      const newCurrencyRate: CurrencyRate = new CurrencyRate();
      newCurrencyRate.value = 'USDtoRWF';
      newCurrencyRate.rates = new Rates();
      newCurrencyRate.rates.RWF = response.rates.USDRWF.rate;
      await this.currencyRateRepository.save(newCurrencyRate);
      const currencyRateResponseDto = new CurrencyRateResponseDto();
      currencyRateResponseDto.rates = new Rates();
      currencyRateResponseDto.rates.RWF = response.rates.USDRWF.rate;
      return currencyRateResponseDto;
    }
  }

  private static getDiffBetween2Dates(date1: string): number {
    const dt1 = new Date(date1);
    const dt2 = new Date();

    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(),
      dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(),
      dt1.getDate()) ) / (1000 * 60 * 60 * 24));
  }
}
