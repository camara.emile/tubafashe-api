import { Column, CreateDateColumn, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity({
  name: 'payment-model',
})
export class PaymentType {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({
    type: 'string',
  })
  name: string;

  @CreateDateColumn()
  createdAt: string;
}
