import { ApiProperty } from '@nestjs/swagger';

export class Rates {
  @ApiProperty({
    nullable: true,
    type: Number,
  })
  USD?: number;

  @ApiProperty({
    nullable: true,
    type: Number,
  })
  XOF?: number;

  @ApiProperty({
    nullable: true,
    type: Number,
  })
  RWF?: number;
}
