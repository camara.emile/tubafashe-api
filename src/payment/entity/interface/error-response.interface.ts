export interface ErrorResponse {
  code: string;
  type: string;
  info: string;
}
