import { Column, CreateDateColumn, Entity, ObjectID, ObjectIdColumn, PrimaryColumn, UpdateDateColumn } from 'typeorm';
import { Rates } from './interface/rate.interface';

@Entity({
  name: 'currency-rate',
})
export class CurrencyRate {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  value: string;

  @Column()
  rates: Rates;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updateAt: string;
}
