import { Column, CreateDateColumn, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity({
  name: 'payment',
})
export class Payment {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({
    type: 'float',
  })
  amount: number;

  @Column({
    type: 'string',
  })
  donatorID: string;

  @Column({
    type: 'string',
  })
  paymentTypeID: string;

  @Column({
    type: 'string',
  })
  authkey: string;

  @Column({
    type: 'string',
  })
  tid: string;

  @Column({
    type: 'string',
  })
  refid: string;

  @Column({
    type: 'string',
  })
  status: string;

  @Column({
    type: 'string',
  })
  campaignID: string;

  @Column({
    type: 'boolean',
  })
  mailIsSend: boolean;

  @CreateDateColumn()
  createdAt: string;
}
