import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { DonatorModule } from './donator/donator.module';
import { CampaignModule } from './campaign/campaign.module';
import { PaymentModule } from './payment/payment.module';
import { AuthenticationMiddleware } from './common/authentication.middleware';
import { CommonModule } from './common/common.module';
import { DatabaseModule } from './database/database.module';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './common/exception/http-exception.filter';
import { CountryModule } from './country/country.module';
import { AuthModule } from './auth/auth.module';

@Module({
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
  imports: [
    CommonModule,
    DatabaseModule,
    DonatorModule,
    CampaignModule,
    PaymentModule,
    CountryModule,
    AuthModule,
  ],
})
export class AppModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
    .apply(AuthenticationMiddleware)
    .forRoutes(
      { path: '/donators', method: RequestMethod.ALL },
      { path: '/campaigns', method: RequestMethod.ALL },
      { path: '/payments', method: RequestMethod.ALL },
      { path: '/countries', method: RequestMethod.ALL },
    );
  }
}
