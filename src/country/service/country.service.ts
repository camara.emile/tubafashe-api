import got, { Got } from 'got';
import * as _ from 'lodash';
import { Injectable } from '@nestjs/common';
import { ConfigBucket } from '../../common/config-bucket';
import { CountryDto } from '../dto/country.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Country } from '../entity/country.entity';

@Injectable()
export class CountryService {
  private readonly restCountriesApiGot: Got;

  constructor(private readonly configBucket: ConfigBucket,
              @InjectRepository(Country)
              private readonly countryRepository: Repository<Country>) {
    this.restCountriesApiGot = got.extend({
      prefixUrl: this.configBucket.REST_COUNTRIES_API,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  public async getCountries() {
    let countriesDto: CountryDto[] = [];
    const countries = await this.countryRepository.find();
    if (_.isEmpty(countries)) {
      countriesDto = await this.callApiAndSave();
    } else {
      countriesDto = CountryService.buildCountryDto(countries);
    }

    return countriesDto;
  }

  public async callApiAndSave(): Promise<CountryDto[]> {
    const countriesDto: CountryDto[] = [];
    const countries: any = await this.restCountriesApiGot.get('').json<[]>();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < countries.length; i++) {
      const country = new Country();
      country.name = countries[i].name.common;
      await this.countryRepository.save(country);
      countriesDto.push(country);
    }

    return countriesDto;
  }

  private static buildCountryDto(countries: Country[]): CountryDto[] {
    const countriesDto: CountryDto[] = [];
    for (const country of countries) {
      countriesDto.push({name: country.name});
    }
    return countriesDto;
  }

}
