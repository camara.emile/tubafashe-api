import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity({
  name: 'country',
})
export class Country {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({
    length: 50,
  })
  name: string;
}
