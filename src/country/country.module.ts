import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Country } from './entity/country.entity';
import { CountryService } from './service/country.service';
import { CountryController } from './controller/country.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Country])],
  providers: [
    CountryService,
  ],
  exports: [CountryService],
  controllers: [CountryController],
})
export class CountryModule {}
