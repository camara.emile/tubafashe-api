import { Controller, Get } from '@nestjs/common';
import { CountryService } from '../service/country.service';
import { CountryDto } from '../dto/country.dto';

@Controller('countries')
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @Get()
  findAll(): Promise<CountryDto[]> {
    return this.countryService.getCountries();
  }
}
