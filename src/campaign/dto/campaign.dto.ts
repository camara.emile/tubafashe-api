import { ApiProperty } from '@nestjs/swagger';

export class CampaignDto {
  id: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  name: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  coverImageUrl: string;

  @ApiProperty({
    nullable: false,
    type: String,
  })
  descriptionFR: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  descriptionEN: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  sharedTextFR: string;

  @ApiProperty({
    nullable: true,
    type: String,
  })
  sharedTextEN: string;

  @ApiProperty({
    nullable: true,
    type: Number,
  })
  amount: number;
  startDate: string;
  endDate: string;

  @ApiProperty({
    nullable: false,
    type: Boolean,
  })
  isTheMain: boolean;

  @ApiProperty({
    nullable: false,
    type: Boolean,
  })
  isActive: boolean;
}
