import { Entity, Column, ObjectIdColumn, ObjectID, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity({
  name: 'campaign',
})
export class Campaign {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({
    length: 50,
  })
  name: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  coverImageUrl: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  descriptionFR: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  descriptionEN: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  sharedTextFR: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  sharedTextEN: string;

  @Column({
    type: 'float',
  })
  amount: number;

  @Column({
    type: 'datetime',
    nullable: false,
  })
  startDate: string;

  @Column({
    type: 'datetime',
    nullable: true,
  })
  endDate: string;

  @Column({
    name: 'is_active',
  })
  isActive: boolean;

  @Column({
    name: 'is_the_main',
  })
  isTheMain: boolean;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;
}
