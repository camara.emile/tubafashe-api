import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Campaign } from '../entity/campaign.entity';
import { CampaignDto } from '../dto/campaign.dto';
import { NotFoundException } from '../../common/exception/notfound.exception';
import { Payment } from '../../payment/entity/payment.entity';

@Injectable()
export class CampaignService {
  constructor(@InjectRepository(Campaign)
              private readonly campaignRepository: Repository<Campaign>) {
  }

  public async create(campaignDto: CampaignDto): Promise<string> {
    await this.updateFieldIsTheMain();
    await this.campaignRepository.save(campaignDto);
    return campaignDto.id;
  }

  public async update(campaignID: string, campaignDto: CampaignDto): Promise<CampaignDto> {
     await this.updateFieldIsTheMain();
     await this.campaignRepository.update(campaignID, campaignDto);

     return campaignDto;
  }

  public async findAll(isTheMain?: string): Promise<Campaign[]> {
    if (isTheMain && isTheMain === 'true') {
      const campaigns = [];
      const campaign = await this.findTheMainCampaign();
      campaigns.push(campaign);
      return campaigns;
    } else {
      return this.campaignRepository.find();
    }
  }

  public async updateCampaignAmount(payment: Payment, amount: number): Promise<void> {
    try {
      const campaign: Campaign = await this.campaignRepository.findOne(payment.campaignID);
      if (!campaign) {
        throw new NotFoundException('Campaign with ID:' + payment.campaignID + ' not found');
      }
      campaign.amount = amount;
      await this.campaignRepository.save(campaign);
    } catch (e) {
      throw new Error(e.toString());
    }
  }

  public async findByPk(campaignID: string): Promise<Campaign> {
    return this.campaignRepository.findOne(campaignID);
  }

  public async findTheMainCampaign(): Promise<Campaign> {
    return this.campaignRepository.findOne({
      where: {
        isTheMain: true,
      },
    });
  }

  public async checkCampaign(campaignID: string): Promise<boolean> {
    try {
      const campaign: Campaign = await this.findByPk(campaignID);

      return !!(campaign);
    } catch (e) {
      throw new Error(e.toString());
    }
  }

  private async updateFieldIsTheMain(): Promise<void> {
    const campaigns = await this.findAll();
    for (const campaign of campaigns) {
      campaign.isTheMain = false;
      await this.campaignRepository.save(campaign);
    }
  }
}
