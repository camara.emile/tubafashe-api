import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Campaign } from './entity/campaign.entity';
import { CampaignService } from './service/campaign.service';
import { CampaignController } from './controller/campaign.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Campaign])],
  providers: [
    CampaignService,
  ],
  controllers: [CampaignController],
})
export class CampaignModule {}
