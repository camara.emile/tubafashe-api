import { Body, Controller, Get, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { CampaignService } from '../service/campaign.service';
import { CampaignDto } from '../dto/campaign.dto';
import { Campaign } from '../entity/campaign.entity';

@Controller('campaigns')
export class CampaignController {
  constructor(private readonly campaignService: CampaignService) {}

  @Get()
  findAll(@Query() params): Promise<Campaign[]> {
    return this.campaignService.findAll(params?.isTheMain);
  }

  @Get(':campaignID')
  async getCampaign(@Param('campaignID') campaignID: string): Promise<Campaign> {
    return this.campaignService.findByPk(campaignID);
  }

  @Post()
  async create(@Body() campaignDto: CampaignDto) {
    return await this.campaignService.create(campaignDto);
  }

  @Put(':campaignID')
  async update(@Param('campaignID') campaignID: string, @Body() campaignDto: CampaignDto) {
    return await this.campaignService.update(campaignID, campaignDto);
  }
}
