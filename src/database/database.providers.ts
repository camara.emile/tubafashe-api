import { Donator } from '../donator/entity/donator.entity';
import { Campaign } from '../campaign/entity/campaign.entity';
import { Payment } from '../payment/entity/payment.entity';
import { PaymentType } from '../payment/entity/payment-type.entity';
import { validate } from '../common/env-validator.decorator';
import { ConfigBucket } from '../common/config-bucket';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CurrencyRate } from '../payment/entity/currency-rate.entity';
import { Country } from '../country/entity/country.entity';
import { User } from '../auth/entity/user.entity';

const configBucket: ConfigBucket = validate<ConfigBucket>(ConfigBucket);
export const databaseProviders = TypeOrmModule.forRoot({
    type: 'mongodb',
    host: configBucket.DATABASE_HOST,
    database: configBucket.DATABASE_NAME,
    username: configBucket.DATABASE_USER,
    password: configBucket.DATABASE_PWD,
    entities: [Campaign, Country, CurrencyRate, Donator, Payment, PaymentType, User],
    synchronize: true,
    useUnifiedTopology: true
  });
