'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">tubafashe-api documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CampaignModule.html" data-type="entity-link">CampaignModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' : 'data-target="#xs-controllers-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' :
                                            'id="xs-controllers-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' }>
                                            <li class="link">
                                                <a href="controllers/CampaignController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CampaignController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' : 'data-target="#xs-injectables-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' :
                                        'id="xs-injectables-links-module-CampaignModule-0799d59f309e77a23b5fe5c3defba7b9"' }>
                                        <li class="link">
                                            <a href="injectables/CampaignService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>CampaignService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CommonModule.html" data-type="entity-link">CommonModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CommonModule-b146a0029beb1676290bffce5677e722"' : 'data-target="#xs-injectables-links-module-CommonModule-b146a0029beb1676290bffce5677e722"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CommonModule-b146a0029beb1676290bffce5677e722"' :
                                        'id="xs-injectables-links-module-CommonModule-b146a0029beb1676290bffce5677e722"' }>
                                        <li class="link">
                                            <a href="injectables/AppLogger.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AppLogger</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DatabaseModule.html" data-type="entity-link">DatabaseModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DonatorModule.html" data-type="entity-link">DonatorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' : 'data-target="#xs-controllers-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' :
                                            'id="xs-controllers-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' }>
                                            <li class="link">
                                                <a href="controllers/DonatorController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DonatorController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' : 'data-target="#xs-injectables-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' :
                                        'id="xs-injectables-links-module-DonatorModule-dbf69c3752ee170d144a8b46cc775f8f"' }>
                                        <li class="link">
                                            <a href="injectables/DonatorService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DonatorService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PaymentModule.html" data-type="entity-link">PaymentModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' : 'data-target="#xs-controllers-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' :
                                            'id="xs-controllers-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' }>
                                            <li class="link">
                                                <a href="controllers/CurrencyController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CurrencyController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/KpayController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">KpayController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/PaymentController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PaymentController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' : 'data-target="#xs-injectables-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' :
                                        'id="xs-injectables-links-module-PaymentModule-54ef45dee4bea515c22e6bb79b293bef"' }>
                                        <li class="link">
                                            <a href="injectables/CurrencyRateService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>CurrencyRateService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/DonatorService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DonatorService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/KpayService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>KpayService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/PaymentService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>PaymentService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link">AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CampaignController.html" data-type="entity-link">CampaignController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CurrencyController.html" data-type="entity-link">CurrencyController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/DonatorController.html" data-type="entity-link">DonatorController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/KpayController.html" data-type="entity-link">KpayController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/PaymentController.html" data-type="entity-link">PaymentController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AuthenticationMiddleware.html" data-type="entity-link">AuthenticationMiddleware</a>
                            </li>
                            <li class="link">
                                <a href="classes/Campaign.html" data-type="entity-link">Campaign</a>
                            </li>
                            <li class="link">
                                <a href="classes/CampaignDto.html" data-type="entity-link">CampaignDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/ConfigBucket.html" data-type="entity-link">ConfigBucket</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateDonatorDto.html" data-type="entity-link">CreateDonatorDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CurrencyRate.html" data-type="entity-link">CurrencyRate</a>
                            </li>
                            <li class="link">
                                <a href="classes/CurrencyRateResponseDto.html" data-type="entity-link">CurrencyRateResponseDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Donator.html" data-type="entity-link">Donator</a>
                            </li>
                            <li class="link">
                                <a href="classes/ForbiddenException.html" data-type="entity-link">ForbiddenException</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link">HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/KpayException.html" data-type="entity-link">KpayException</a>
                            </li>
                            <li class="link">
                                <a href="classes/KpayPayloadDto.html" data-type="entity-link">KpayPayloadDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/KpayPaymentRequestDto.html" data-type="entity-link">KpayPaymentRequestDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/KpayPaymentResponseDto.html" data-type="entity-link">KpayPaymentResponseDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/NotFoundException.html" data-type="entity-link">NotFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/Payment.html" data-type="entity-link">Payment</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaymentDto.html" data-type="entity-link">PaymentDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaymentType.html" data-type="entity-link">PaymentType</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaymentTypeDto.html" data-type="entity-link">PaymentTypeDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Rates.html" data-type="entity-link">Rates</a>
                            </li>
                            <li class="link">
                                <a href="classes/ServiceUnavaibleException.html" data-type="entity-link">ServiceUnavaibleException</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppLogger.html" data-type="entity-link">AppLogger</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link">AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CampaignService.html" data-type="entity-link">CampaignService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CurrencyRateService.html" data-type="entity-link">CurrencyRateService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DonatorService.html" data-type="entity-link">DonatorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/KpayService.html" data-type="entity-link">KpayService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PaymentService.html" data-type="entity-link">PaymentService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ErrorResponse.html" data-type="entity-link">ErrorResponse</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});